package org.amicofragile.learning.eventstore;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import eventstore.*;
import eventstore.j.*;
import eventstore.tcp.ConnectionActor;

import java.net.InetSocketAddress;
import java.util.UUID;

public class HelloEventStore {
    public static void main(String[] args) {
        final Settings settings = new SettingsBuilder()
                .address(new InetSocketAddress("127.0.0.1", 1113))
                .defaultCredentials("admin", "changeit")
                .build();
        final ActorSystem system = ActorSystem.create();
        final ActorRef connection = system.actorOf(ConnectionActor.getProps(settings));
        final ActorRef writeResult = system.actorOf(Props.create(WriteResult.class));

        final EventData event0 = new EventDataBuilder("my-event")
                .eventId(UUID.randomUUID())
                .jsonData("{\"a\": \"1\", \"text\": \"Hello, EventStore!\"}")
                .metadata("my first event: 0")
                .build();

        final EventData event1 = new EventDataBuilder("my-event")
                .eventId(UUID.randomUUID())
                .jsonData("{\"a\": \"2\", \"text\": \"Hello again, EventStore!\"}")
                .metadata("my second event: 1")
                .build();


        final WriteEvents writeEvents = new WriteEventsBuilder("HelloTestStream")
                .addEvent(event0)
                .addEvent(event1)
                .expectAnyVersion()
                .build();

        //connection.tell(writeEvents, writeResult);

        final ActorRef readResult = system.actorOf(Props.create(ReadResult.class));

//        final ReadEvent readEvent = new ReadEventBuilder("HelloTestStream")
//                //.first()
//                .resolveLinkTos(false)
//                .build();

        final ReadStreamEvents readAll = new ReadStreamEventsBuilder("HelloTestStream")
                .forward()
                .fromFirst()
                .build();

        connection.tell(readAll, readResult);
    }

    public static class WriteResult extends UntypedActor {
        final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

        public void onReceive(Object message) throws Exception {
            log.info("message tye: {}", message.getClass().getCanonicalName());
            if (message instanceof WriteEventsCompleted) {
                final WriteEventsCompleted completed = (WriteEventsCompleted) message;
                log.info("range: {}, position: {}", completed.numbersRange(), completed.position());
            } else if (message instanceof Status.Failure) {
                final Status.Failure failure = ((Status.Failure) message);
                final EsException exception = (EsException) failure.cause();
                log.error(exception, exception.toString());
            } else
                unhandled(message);

            context().system().terminate();
        }
    }

    public static class ReadResult extends UntypedActor {
        final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

        public void onReceive(Object message) throws Exception {
            if (message instanceof ReadStreamEventsCompleted) {
                final ReadStreamEventsCompleted completed = (ReadStreamEventsCompleted) message;
                log.info("direction: {}, last event number: {}", completed.direction(), completed.lastEventNumber());
                log.info("Events count: {}", completed.eventsJava().size());
                for(Event evt : completed.eventsJava()) {
                    log.info("event: {} data: {}", evt.number(), evt.data().data().toString());
                }
            } else if (message instanceof Status.Failure) {
                final Status.Failure failure = ((Status.Failure) message);
                final EsException exception = (EsException) failure.cause();
                log.error(exception, exception.toString());
            } else
                unhandled(message);

            context().system().terminate();
        }
    }
}
